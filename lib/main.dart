import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late GoogleMapController mapController;

  final LatLng _center = const LatLng(7.519102443560451, 99.57896245728163);

  Future<void> _onMapCreated(GoogleMapController controller) async{
    mapController = controller;
    String value = await DefaultAssetBundle.of(context)
        .loadString('assets/mapstyle.json');
    mapController.setMapStyle(value);
  }
  Set<Marker> _createMarker() {
    return {
      Marker(
          markerId: MarkerId("marker_1"),
          position: _center,
          infoWindow: InfoWindow(title: 'ม.อ.ตรัง', snippet: 'มหาวิทยาลัยสงขลานครินทร์ วิทยาเขตตรัง'),
          //rotation: 90,
          icon: BitmapDescriptor.defaultMarker,
      ),
      Marker(
        markerId: MarkerId("marker_2"),
        position: LatLng(8.872563032279947, 98.36248714014083),
        infoWindow: InfoWindow(title: 'บ้านของนางสาววรลักษณ์', snippet: '23/13 ม.1 ต.โคกเคียน อ.ตะกั่วป่าจ.พังงา'),
        //rotation: 90,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
      ),
    };
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Google Maps'),
          backgroundColor: Colors.greenAccent[700],

        ),
        body: GoogleMap(
          myLocationEnabled: true,
          mapToolbarEnabled: true,
          //mapType: MapType.satellite,
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 15.0,
          ),
          markers: _createMarker(),
        ),
      ),
    );
  }
}
